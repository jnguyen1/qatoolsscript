import requests
import jenkins
import xml.etree.ElementTree as ET
import re
import json

schema_dict = {}
dbPort = "1521"

def getUpstreamProject(smoketest_jobs):
    upstream_projects = []
    for job in smoketest_jobs:
        response = requests.get("http://jenkins:8080/view/QA_Nightly_Builds/view/SmokeTests/job/%s/api/"
                                "json?pretty=true&tree=upstreamProjects[name],views[name,upstreamProjects[name]]" % job)
        data = response.json()
        if len(data['upstreamProjects']) != 0:
            temp = data['upstreamProjects'][0]['name']
            upstream_projects.append(temp)
    return upstream_projects
# end getUpstreamProject()

def createConfigFile(upstreamProjects):
    for project in upstreamProjects:
        if "Master_Wrapper" not in project:
            # user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
            url = "http://jenkins:8080/view/QA_Nightly_Builds/view/SmokeTests/"
            j = jenkins.Jenkins(url, username = 'jnguyen', password='7b290c22a6a04b675b54a80124ff09b4')

            config_xml = j.get_job_config(project)
            # print (config_xml)

            tree = ET.fromstring(config_xml)

            goal = tree.find('goals')
            dbInfo = goal.text
            # print (dbInfo)
            addDbInfoToJSON(project, dbInfo)

            for node in tree.iter('command'):
                curl_str = node.text

                if 'curl' in curl_str:
                    addUrlInfoToJSON(project, curl_str)
# end createConfigFile()

def addUrlInfoToJSON(project, curl_str):
    urlStr = re.search('@[^\/]+', curl_str).group(0)
    urlStr = 'http://' +urlStr[1:] + '/' + project
    # print(urlStr)
    schema_dict[project]["url"] = urlStr

#end addUrlInfoToJSON()

def addDbInfoToJSON(project, dbInfo):
    subStrDbInfo = dbInfo.split(' ')
    # print (subStrDbInfo)

    dbServer = next(s for s in subStrDbInfo if "dbServer" in s)
    dbServer = dbServer[dbServer.index('=')+1 : len(dbServer)]
    dbServer = dbServer[1:]
    # print (dbServer)

    dbService = next(s for s in subStrDbInfo if "dbService" in s)
    dbService = dbService[dbService.index('=') + 1: len(dbService)]
    # print (dbService)

    schemaName = next(s for s in subStrDbInfo if "schemaName" in s)
    schemaName = schemaName[schemaName.index('=') + 1: len(schemaName)]
    # print (schemaName)


    global schema_dict
    schema_dict[project] = {'schemaName': schemaName ,'dbServer': dbServer, 'dbService': dbService, 'dbPort' : dbPort}
#end storeDbInfo()


response = requests.get('http://jenkins:8080/view/QA_Nightly_Builds/view/SmokeTests/api/json?pretty=true&tree=jobs[name],views[name,jobs[name]]')

data = response.json()

smoketest_jobs = [job['name'] for job in data['jobs']]
# print (smoketest_jobs)

upstreamProjects = getUpstreamProject(smoketest_jobs)

# print (upstreamProjects)

createConfigFile(upstreamProjects)

with open('nightly_schemas.json', 'w') as fp:
    json.dump(schema_dict, fp)
print (schema_dict)
